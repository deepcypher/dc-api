.. _docker: https://www.docker.com/
.. |docker| replace:: Docker

.. _docker-compose: https://docs.docker.com/compose/
.. |docker-compose| replace:: Docker-Compose

.. _bash shell: https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
.. |bash shell| replace:: Bash shell

.. _manifest: https://kubernetes.io/docs/reference/kubectl/cheatsheet/#creating-objects
.. |manifest| replace:: Manifest

.. _helm: https://helm.sh/
.. |helm| replace:: Helm

.. |username| replace:: ``username``
.. |group| replace:: ``group``
.. |csr| replace:: ``csr``

.. |section_fhe| replace:: :ref:`section_fhe`
.. |fhe| replace:: :ref:`section_fhe`

.. |section_optimizers| replace:: :ref:`section_optimizers`
.. |section_activations| replace:: :ref:`section_activations`
.. |section_layers| replace:: :ref:`section_layers`
.. |section_blocks| replace:: :ref:`section_blocks`

.. |section_cnn| replace:: :ref:`section_cnn`
.. |section_cnn_equations| replace:: :ref:`section_cnn_equations`
.. |section_commuted_sum| replace:: :ref:`section_commuted_sum`
.. |section_hadmard_product| replace:: :ref:`section_hadmard_product`
.. |section_masquerade| replace:: :ref:`section_masquerade`

.. |section_sigmoid| replace:: :ref:`section_sigmoid`
.. |section_sigmoid_approx| replace:: :ref:`section_sigmoid_approximation`
.. |section_sigmoid_deriv| replace:: :ref:`section_sigmoid_derivative`
.. |section_sigmoid_approx_deriv| replace:: :ref:`section_sigmoid_approximation_derivative`

.. |section_relu| replace:: :ref:`section_relu`
.. |section_relu_approx| replace:: :ref:`section_relu_approximation`
.. |section_relu_deriv| replace:: :ref:`section_relu_derivative`
.. |section_relu_approx_deriv| replace:: :ref:`section_relu_approximation_derivative`

.. |section_docker_build| replace:: :ref:`section_docker_build`

.. |api-build-note| replace:: You may see some or no content at all on this page that documents the API. If that is the case please build the documentation locally (|section_docker_build|), as we have yet to find a good way to build these complex libraries on RTDs servers.
