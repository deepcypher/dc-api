.. include:: substitutions

DeepCypher-API
==============

The DeepCypher API is the core encryption, decryption, and task-queuing mechanism for DeepCypher.
The API is the core component used by the front-end to securely, and uniformly access and process encrypted data, without elevating the privileges of the front-end at all, to minimize attack vectors further even in-cluster.

.. toctree::
  :maxdepth: 1
  :caption: Table of Contents
  :numbered:

  license
  install
