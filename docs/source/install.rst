.. pyrtd documentation master file, created by
   sphinx-quickstart on Mon Aug 26 13:30:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Installation
############

Currently due to a certain amount of awkwardness of the python to Microsoft-SEAL bindings, the only "supported" installation method is by docker container.
However we are working on fixing some of these issues in the near future, or at the very least supporting a second pure-python backend so that installation is easier as a native library if that is desired.

Docker Registry
+++++++++++++++

Dockerised images that you can play around with are available at: https://gitlab.com/deepcypher/dc-api/container_registry

E.G, download and run interactively:

.. code-block:: bash

  docker run -it registry.gitlab.com/deepcypher/dc-api:master

.. _section_docker_build:

Docker Build
++++++++++++

If you would rather build this library locally/ for security reasons, you can issue the following build command from inside the projects root directory:

.. code-block:: bash

  docker build -t dc/api -f Dockerfile_archlinux .

.. note::

  The build process could take several minutes to build, as it will compile, bind, and package the Microsoft-SEAL library in with itself.

Docker Documentation Build
--------------------------

To build the detailed documentation with all the autodoc functionality for the core classes, you can use your now built container to create these docs for you by:

.. code-block:: bash

  docker run -v ${PWD}/docs/build/:/app/docs/build -it dc/api make -C /app/docs html

The docs will then be available in ``${PWD}/docs/build/html`` for your viewing pleasure

Locally-Built Docker-Image Interactive Usage
--------------------------------------------

To run the now locally built container you can issue the following command to gain interactive access, by selecting the tag (-t) that you named it previously (here it is dc/api):

.. code-block:: bash

  docker run -it dc/api

Helm Chart
++++++++++

We simultaneously use our gitlab pages to host our latest documentation and our latest helm-charts, I.E https://deepcypher.gitlab.io/dc-api thus you can add the repo simply:

.. code-block:: bash

  helm repo add dc-api-pages https://deepcypher.gitlab.io/dc-api

Then you can download the latest helm-chart tarball using:

.. code-block:: bash

  helm repo update

Finally you can deploy the helm chart to an existing kubernetes cluster by issuing the following:

.. code-block:: bash

  helm install dc-api dc-api-pages/dc-api
