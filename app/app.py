#!/usr/bin/env python3

# @Author: GeorgeRaven <archer>
# @Date:   2020-06-17T15:30:36+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-17T16:42:44+01:00
# @License: please see LICENSE file in project root

import os
import logging
import flask


def gen_random(len=60):
    import random
    import string
    x = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
                for _ in range(len))
    logging.debug("secret_key: {}".format(x))
    return x


def main():
    """Generate App / App factory."""
    app = flask.Flask(__name__, template_folder=None, static_folder=None)
    secret_key = os.environ.get("SECRET_KEY")
    app.secret_key = secret_key if secret_key is not None else gen_random()
    name = os.environ.get("SERVER_NAME")
    name = name if name is not None else "deepcypher.me"
    app.config["SERVER_NAME"] = name
    # os.urandom(256)
    return app


def entry_point():
    """Launch given app."""
    from api.api import api_main
    app = main()
    logging.info("SERVER_NAME: {}".format(app.config["SERVER_NAME"]))
    # generating API object
    api = api_main()
    # registering blueprint so we can test it if this file is run alone
    app.register_blueprint(api, subdomain="api")
    logging.debug(app.config)

    logging.info("Launching flask api server...")
    logging.info("Routes: \n{}".format(app.url_map))
    return app


if __name__ == "__main__":
    logging.basicConfig(  # filename="./flask.log",
        level=logging.DEBUG,
        format="%(asctime)s %(levelname)s:%(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S")
    app = entry_point()
    # changing to dev address cryptologdev.io in /etc/hosts should be localhost
    app.run(
        host="localhost",
        port=443,
        debug=True,
        ssl_context="adhoc"  # gaff https
    )
else:
    logging.basicConfig(  # filename="/flask/flask.log",
        level=logging.INFO,
        format="%(asctime)s %(levelname)s:%(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S")
