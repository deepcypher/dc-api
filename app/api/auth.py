from passlib.hash import argon2
import unittest


class Auth(object):

    def __init__(self, password=None, hash=None):
        if hash:
            self._hash = hash
        self._password = password

    def verify(self, password=None):
        hasher = self.hasher
        hash = self.hash
        password = password if password is not None else self.password
        return hasher.verify(password, hash)
        # # result = self.hasher.verify(password, self.hash)
        # return result

    @property
    def hasher(self):
        if self.__dict__.get("_hasher"):
            return self._hasher
        else:
            self._hasher = argon2.using(type="ID")
            return self.hasher

    @property
    def hash(self):
        if self.__dict__.get("_hash"):
            return self._hash
        else:
            self._hash = self.hasher.hash(self.password)
            return self.hash

    @hash.setter
    def hash(self, hash):
        self._hash = hash

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password


class AuthTest(unittest.TestCase):

    def test_init(self):
        self.assertIsInstance(Auth(), Auth)

    def test_hash_verify_auto(self):
        word = "IdDoAnythingForLoveButIWontDoThat"
        a = Auth(password=word)
        output = a.verify(word)
        self.assertTrue(output)
        # WTF why does the below version not work when the above is 2 lines
        # are removed??????
        output = a.hasher.verify(word, a.hash)
        self.assertTrue(output)

        self.assertTrue(a.hasher.verify(word, a.hash))


if __name__ == "__main__":
    # run all the unit-tests
    print("now testing:", __file__, "...")
    unittest.main()
