# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T12:07:00+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-16T12:14:50+01:00
# @License: please see LICENSE file in project root
import flask_restful  # for api
from flask_restful import reqparse  # for argument parsing
from ezdb.mongo import Mongo  # mongodb handler
from .users import is_authenticated, get_user
from .args import arg_handler
import logging as logger
import bson


class User_Data(flask_restful.Resource):
    def get(self):
        argparser = reqparse.RequestParser()
        argparser.add_argument("pass", type=str, required=True,
                               help="password for authentication required")
        argparser.add_argument("name", type=str, required=True,
                               help="username for authentication required")
        args = argparser.parse_args()
        authenticated = is_authenticated(args["name"], args["pass"])
        if authenticated:
            user = get_user(args)
            logger.debug("USER DATA GET: {}".format(str(user)))
            db = Mongo(arg_handler())
            pipeline = [
                {'$match': {'owner': bson.objectid.ObjectId(user["id"])}},
                {
                    '$project': {
                        '_id': 1,
                        'set': 1,
                        'datetime': 1,
                        'comment': 1
                    }
                },
                {'$sort': {'datetime': -1}}
            ]
            data = []
            db.connect()
            cursor = db.getCursor(db_collection_name="data",
                                  db_pipeline=pipeline)
            for batch in db.getBatches(db_data_cursor=cursor):
                for item in batch:
                    item["datetime"] = str(item["datetime"])
                    item["id"] = str(item["_id"])
                    del item["_id"]
                    data.extend([item])
            user["data"] = data
        else:
            return {"message":
                    "error, user '{}' failed authentication".format(
                        args["name"])}, 403
        return user
