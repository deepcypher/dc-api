import logging as logger
import flask
try:
    import flask_restful  # for api
except ImportError:
    # Monkey patch to fix flask internal issues
    # https://stackoverflow.com/questions/67496857/cannot-import-name-endpoint-from-view-func-from-flask-helpers-in-python/67523704#67523704
    flask.helpers._endpoint_from_view_func = \
        flask.scaffold._endpoint_from_view_func
    import flask_restful  # for api
import marshmallow as ma
from webargs.flaskparser import use_args, use_kwargs


class Ready(flask_restful.Resource):
    """Status display and liveness testing class."""

    @use_kwargs({}, location="json")
    def get(self):
        """Get current status of server."""
        logger.info("ready")
        return {
            "ready": True
        }

    @use_kwargs({"response": ma.fields.Int(required=True)}, location="json")
    def put(self, response):
        """Put int to check responsiveness."""
        logger.info("ready")
        return {
            "ready": True,
            "ack": response
        }
