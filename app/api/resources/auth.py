import time
import logging as logger
import flask
try:
    import flask_restful  # for api
except ImportError:
    # Monkey patch to fix flask internal issues
    # https://stackoverflow.com/questions/67496857/cannot-import-name-endpoint-from-view-func-from-flask-helpers-in-python/67523704#67523704
    flask.helpers._endpoint_from_view_func = \
        flask.scaffold._endpoint_from_view_func
    import flask_restful  # for api
import marshmallow as ma
from webargs.flaskparser import use_args, use_kwargs
from ezdb.mongo import Mongo


class Authenticate(flask_restful.Resource):
    """Status display and liveness testing class."""

    def limit(min=1, max=5):
        """Decorator factory to set rate limits"""
        def decorate(fn):
            """Rate limit decorator to both obfiscate and prevent brute force."""
            def wrapper(*args, **kwargs):
                """Wrapper for function arguments"""
                start_time = time.time()  # start timer
                result = fn(*args, **kwargs)  # call the function
                delta = (start_time+min)-start_time
                logger.info("delta: {}".format(delta))
                if time.time() < (start_time + min):
                    # if time is less than minimum
                    time.sleep((start_time+min)-time.time())
                elif time.time() < (start_time + max) and \
                        time.time() > (start_time + min):
                    # if time is between minimum and maximum I.E ideal
                    pass
                else:
                    # if time exceeds minimum time
                    # TODO: implement as while time has not exceeded
                    # as this method will timeout after getting a response
                    # eventually which may or may not be the case
                    flask_restful.abort(504, errors="Auth timeout")
                return result
            return wrapper
        return decorate

    @use_kwargs({}, location="json")
    @limit()
    def get(self, username=None, password=None):
        """Get the permissions and whether the user is authenticated on the server."""
        logger.info("status")
        return {
            "healthy": True
        }

    @use_kwargs({"response": ma.fields.Int(required=True)}, location="json")
    @limit()
    def put(self, response):
        """Create a user authenticated on the server."""
        self.start_time = time.time()  # timer
        logger.info("status")
        return {
            "healthy": True,
            "ack": response
        }
