# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T12:07:49+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-18T13:37:03+01:00
# @License: please see LICENSE file in project root
import flask_restful  # for api
from flask_restful import reqparse  # for argument parsing
from ezdb.mongo import Mongo  # mongodb handler
from .cnn import Layer_CNN  # CNN implementation
from .users import is_authenticated, get_user
from .args import arg_handler
import logging as logger
from fhe.reseal import Reseal, ReScheme
import bson


class Train(flask_restful.Resource):
    """Select and run data through an FHE compatible neural network."""

    def put(self):
        argparser = reqparse.RequestParser()
        argparser.add_argument("pass", type=str, required=True,
                               help="password for authentication required")
        argparser.add_argument("name", type=str, required=True,
                               help="username for authentication required")
        argparser.add_argument("ids", type=list, default=None,
                               help="List of ids to use if no set provided")
        argparser.add_argument("set", type=str, default=None,
                               help="Data set name to use.")
        argparser.add_argument("sequence_length", type=int, default=1,
                               help="set sequence length for neural net")
        argparser.add_argument("batch_size", type=int, default=1,
                               help="set batch size for neural network")
        args = argparser.parse_args()
        logger.debug("INFER: start")
        logger.debug("INFER: input {}".format(args))
        authenticated = is_authenticated(args["name"], args["pass"])
        if authenticated:
            # setting up the database pipelines based on user and request
            user = get_user(args)
            pipeline = [
                # this will ensure only data owned by this user is collected
                {'$match': {'owner': bson.objectid.ObjectId(user["id"])}},
            ]
            # check that either ids or set has been provided
            if args["ids"]:
                stage_dicts = []
                for i in args["ids"]:
                    stage_dicts.append({"_id": bson.objectid.ObjectId(i)})
                pipeline.append({'$or': stage_dicts})
            elif args["set"]:
                pipeline.append({'$match': {'set': args["set"]}})
            else:
                return {
                    "code": "infer-set-ids",
                    "message":
                    "error, data `set` nor `ids` have been specified"}, 400
        else:
            return {"message":
                    "error, user '{}' failed authentication".format(
                        args["name"])}, 403

        logger.debug("INFER pipeline: {}".format(pipeline))
        db = Mongo(arg_handler())
        db.connect()
        cursor = db.getCursor(db_collection_name="data", db_pipeline=pipeline)
        cnn = None
        processed_ids = []  # keeping track of what was in each batch
        y_hats = []  # results of each batch
        # get the batches of data from the database
        # TODO this cannot guarantee the order of retrieval in this state
        # so I may need to add something to enable users to specify the order
        for batch in db.getBatches(
                db_data_cursor=cursor,
                db_batch_size=args["sequence_length"]*args["batch_size"]):
            batch_ids = []  # keeping track of what was in this batch
            data_batch = []  # this is the deserialised dataset
            for doc in batch:
                logger.debug("INFER doc keys: {}".format(doc.keys()))
                batch_ids.append(doc["_id"])
                r_dict = ReScheme().load(doc["data"])
                r = Reseal()
                r.__setstate__(r_dict)
                # logger.debug("INFER r: {} {}".format(r, str(len(r))))
                data_batch.append(r)
            if cnn is None:
                weight_shape = (args["batch_size"],  # batches
                                args["sequence_length"],  # filter height
                                1,  # filter width
                                len(r))
                # logger.debug("INFER weight shape: {}".format(weight_shape))
                cnn = Layer_CNN(weights=weight_shape, bias=0)
            y_hat = cnn.forward(data_batch)  # apply convolution and activation
            logger.debug("INFER y_hat: {}, type: {}".format(y_hat,
                                                            type(y_hat)))
            y_hat = y_hat.__getstate__()  # serialise
            y_hat = ReScheme().dumps(y_hat)  # jsonify
            # logger.debug("INFER batch retrieve: {}".format(type(data_batch)))
            y_hats.append(y_hat)
            processed_ids.append(batch_ids)

        return {"message": "success",
                "y_hats": y_hats,
                # "ids": processed_ids,
                }
