# @Author: GeorgeRaven <archer>
# @Date:   2020-09-18T13:28:53+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-18T13:31:09+01:00
# @License: please see LICENSE file in project root

import logging as logger
import numpy as np
from fhe.reseal import Reseal


class CNN(object):
    # adapted reference:
    # https://www.coursera.org/specializations/deep-learning
    # https://towardsdatascience.com/lets-code-convolutional-neural-network-in-plain-numpy-ce48e732f5d5

    def __init__(self,
                 nn_weights=None,
                 nn_bias=None,
                 nn_activation_function=None,
                 sequence_length=None,
                 batch_size=None,
                 stride=None):
        self._bias = nn_bias if nn_bias is not None else None
        self.weights = nn_weights if nn_weights is not None else None
        self._activation_function = nn_activation_function if \
            nn_activation_function is not None else self.polynomial_sigmoid
        self._sequence_length = sequence_length if sequence_length is not \
            None else 1
        self._batch_size = batch_size if batch_size is not None else 1
        self._stride = stride if stride is not None else 1

    def __str__(self):
        return str(self.__dict__)

    # https://eprint.iacr.org/2018/462.pdf
    def polynomial_sigmoid(self, x):
        """ Sigmoid polynomial approximator: 0.5 + 0.197x + -0.004x^3"""
        return 0.5 + (0.197 * x) + ((-0.004 * x) * (x * x))

    @property
    def bias(self):
        return self._bias

    @bias.setter
    def bias(self, bias):
        self._bias = bias

    @property
    def weights(self):
        return self._weights

    @property
    def activation_function(self):
        return self._activation_function

    @weights.setter
    def weights(self, weights):
        # initialise weights from tuple dimensions
        # TODO: properly implement xavier weight initialisation over np.rand
        if isinstance(weights, tuple):
            # https://www.coursera.org/specializations/deep-learning
            # https://towardsdatascience.com/weight-initialization-techniques-in-neural-networks-26c649eb3b78
            self._weights = np.random.rand(*weights)
        else:
            self._weights = weights

    def _forward(self, x_batch):
        logger.debug("CNN batch: {}".format(x_batch))
        sum = None
        for t in range(len(x_batch)):
            # logger.debug("CNN original: {}".format(x_batch[t].plaintext))
            # logger.debug("CNN weights: {}".format(self.weights[0][t][0]))
            convolution = x_batch[t] * self.weights[0][t][0]
            # logger.debug("CNN convolution: {}".format(convolution.plaintext))
            if sum is None:
                sum = convolution
            else:
                sum = sum + convolution
        biased = sum + self.bias
        return self.activation_function(biased)

    def _backward(self, activation):
        # https://www.analyticsvidhya.com/blog/2020/02/mathematics-behind-convolutional-neural-network/
        # https://www.mathsisfun.com/calculus/derivatives-rules.html
        # I always forget the rules ^
        # new_parameter = old_parameter - (learning_rate * gradient_of_parameter)
        pass

    def _update(self):
        pass

    def _generate_batches(self, x, y, batch_size):
        """From full data set, yield bite sized batches."""
        for i in range(2):
            yield np.zeros((3, 1, 4096))

    def train(self, x_train: np.array,
              y_train: np.array,
              epochs: int = 1,
              batch_size: int = 1):
        """Standard training with special generator to retrieve from db."""
        for epoch in range(epochs):
            for x_batch, y_batch in self._generate_batches(
                    x=x_train,
                    y=y_train,
                    batch_size=batch_size):
                y_hat_batch = self._forward(x_batch)
                loss = y_hat_batch - y_batch
                self._backward(loss)
                self._update()

    def predict(self, x_batch):
        """Standard inference given batch"""
        return self._forward(x_batch)
