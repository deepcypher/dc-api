# @Author: GeorgeRaven <archer>
# @Date:   2020-08-29T20:42:43+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-16T21:05:50+01:00
# @License: please see LICENSE file in project root


import flask
try:
    import flask_restful  # for api
except ImportError:
    # Monkey patch to fix flask internal issues
    # https://stackoverflow.com/questions/67496857/cannot-import-name-endpoint-from-view-func-from-flask-helpers-in-python/67523704#67523704
    flask.helpers._endpoint_from_view_func = \
        flask.scaffold._endpoint_from_view_func
    import flask_restful  # for api
from webargs.flaskparser import parser, abort
from .args import arg_handler
from .resources.status import Status
from .resources.ready import Ready
from .resources.auth import Authenticate
import logging as logger


def api_main():
    api_bp = flask.Blueprint(
        "api", __name__,
        template_folder="templates", static_folder="static")

    api = flask_restful.Api(api_bp)

    api.add_resource(Status, "/status")
    api.add_resource(Ready, "/ready")
    api.add_resource(Authenticate, "/auth")

    return api_bp


@parser.error_handler
def handle_request_parsing_error(err, req, schema, *, error_status_code, error_headers):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    logger.error(
        "Request:{}, error:{}, schema:{}, status:{}, error_header:{}".format(
            req, err, schema, error_status_code, error_headers))
    if error_status_code is not None:
        abort(error_status_code, errors=err.messages)
    else:
        abort(400, errors=err.messages)


if __name__ == "__main__":
    app = flask.Flask(__name__)
    # generating API object
    api = api_main()
    # registering blueprint so we can test it if this file is run alone
    app.config["SERVER_NAME"] = "deepcypher.me"
    # app.config["SERVER_NAME"] = "localhost:8000"
    app.register_blueprint(api, subdomain="api")
    # print(app.config)
    # print(app.url_map)
    app.run(
        host="localhost",
        port=443,
        debug=True,
        ssl_context="adhoc"  # gaff https
    )
