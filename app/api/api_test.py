#!/usr/bin/env python3

# @Author: GeorgeRaven <archer>
# @Date:   2020-08-18T11:33:45+01:00
# @Last modified by:   archer
# @Last modified time: 2021-01-13T16:45:57+00:00
# @License: please see LICENSE file in project root


from fhe.reseal import Reseal, ReScheme
import os
import time
import marshmallow
import requests
import unittest
import numpy as np
import string
import random
import webargs
# disabling the plethora of warning messages that can be shot out in testing
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class Status(unittest.TestCase):
    """Check Status API class."""

    def setUp(self):
        """Set up timers."""
        self.start_time = time.time()

    def tearDown(self):
        """Consume timers."""
        t = time.time() - self.start_time
        print('%s: %.3f' % (self.id(), t))

    @property
    def url(self):
        """Get URL to server."""
        # url = "http://api.deepcypher.me"
        name = os.environ.get("SERVER_BASE_URL")
        name = name if name is not None else "https://api.deepcypher.me"
        url = name  # "http://api.deepcypher.me"
        return url

    @property
    def uri(self):
        """Get URI to resource."""
        uri = "status"
        return uri

    def test_get(self):
        """Check that the api is responsive."""
        url = "{}/{}".format(self.url, self.uri)
        response = requests.get(
            url,
            verify=False)
        if not response:
            print(url, response.content)
        self.assertTrue(response)

    def test_put(self):
        """Check that the api is responsive."""
        req_data = {
            "response": 3,
        }
        url = "{}/{}".format(self.url, self.uri)
        response = requests.put(
            url,
            # data=req_data, # webargs considers as "FORM" DATA
            json=req_data,  # webargs considers as "JSON" DATA
            verify=False)
        if not response:
            print(url, response.content)
        self.assertTrue(response)


class Ready(unittest.TestCase):
    """Check Status API class."""

    def setUp(self):
        """Set up timers."""
        self.start_time = time.time()

    def tearDown(self):
        """Consume timers."""
        t = time.time() - self.start_time
        print('%s: %.3f' % (self.id(), t))

    @property
    def url(self):
        """Get URL to server."""
        # url = "http://api.deepcypher.me"
        name = os.environ.get("SERVER_BASE_URL")
        name = name if name is not None else "https://api.deepcypher.me"
        url = name  # "http://api.deepcypher.me"
        return url

    @property
    def uri(self):
        """Get URI to resource."""
        uri = "ready"
        return uri

    def test_get(self):
        """Check that the api is responsive."""
        url = "{}/{}".format(self.url, self.uri)
        response = requests.get(
            url,
            verify=False)
        if not response:
            print(url, response.content)
        self.assertTrue(response)

    def test_put(self):
        """Check that the api is responsive."""
        req_data = {
            "response": 3,
        }
        url = "{}/{}".format(self.url, self.uri)
        response = requests.put(
            url,
            # data=req_data, # webargs considers as "FORM" DATA
            json=req_data,  # webargs considers as "JSON" DATA
            verify=False)
        if not response:
            print(url, response.content)
        self.assertTrue(response)


class Authenticate(unittest.TestCase):
    """Check Status API class."""

    def setUp(self):
        """Set up timers."""
        self.start_time = time.time()

    def tearDown(self):
        """Consume timers."""
        t = time.time() - self.start_time
        print('%s: %.3f' % (self.id(), t))

    @property
    def url(self):
        """Get URL to server."""
        # url = "http://api.deepcypher.me"
        name = os.environ.get("SERVER_BASE_URL")
        name = name if name is not None else "https://api.deepcypher.me"
        url = name  # "http://api.deepcypher.me"
        return url

    @property
    def uri(self):
        """Get URI to resource."""
        uri = "auth"
        return uri

    def test_get(self):
        """Check that the api is responsive."""
        url = "{}/{}".format(self.url, self.uri)
        response = requests.get(
            url,
            verify=False)
        if not response:
            print(url, response.content)
        self.assertTrue(response)

    def test_put(self):
        """Check that the api is responsive."""
        req_data = {
            "response": 3,
        }
        url = "{}/{}".format(self.url, self.uri)
        response = requests.put(
            url,
            # data=req_data, # webargs considers as "FORM" DATA
            json=req_data,  # webargs considers as "JSON" DATA
            verify=False)
        if not response:
            print(url, response.content)
        self.assertTrue(response)


if __name__ == "__main__":
    # run all the unit-tests
    print("now testing:", __file__, "...")
    unittest.main()
