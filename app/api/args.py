# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T12:00:52+01:00
# @Last modified by:   archer
# @Last modified time: 2021-01-15T14:41:54+00:00
# @License: please see LICENSE file in project root

import configargparse


def arg_handler(argv=[], description=None):
    """Quick argument handler just to make everything easier to work with."""
    description = description if description is not None else "api args"
    parser = configargparse.ArgumentParser(description=description)
    parser.add_argument("-i", "--db-ip",
                        type=str,
                        default="database-service",
                        env_var="API_DB_IP",
                        help="IP or hostname where db is accessible.")
    parser.add_argument("-p", "--db-port",
                        type=int,
                        default=27017,
                        env_var="API_DB_PORT",
                        help="Port where db is accessible.")
    parser.add_argument("-A", "--db-authentication",
                        type=str,
                        default="",  # will not use authentication if empty
                        env_var="API_DB_AUTHENTICATION",
                        help="Db authentication to use or empty for none.")
    parser.add_argument("-n", "--db-user-name",
                        type=str,
                        default="default-username",
                        env_var="API_DB_USER_NAME",
                        help="Database username to use.")
    parser.add_argument("-P", "--db-password",
                        type=str,
                        default="default-password",
                        env_var="API_DB_USER_PASSWORD",
                        help="Database user password for authentication.")
    parser.add_argument("-T", "--db-tls",
                        type=bool,
                        default=False,  # will not use tls by default
                        env_var="API_DB_TLS",
                        help="Is databse TLS enabled, false for no.")
    parser.add_argument("-c", "--db-tls-ca-file",
                        type=str,
                        default="/etc/ssl/db.cert",
                        env_var="API_DB_TLS_CA_FILE",
                        help="Certificate authority file to use if any.")
    parser.add_argument("-N", "--db-name",
                        type=str,
                        default="deepcypher",
                        env_var="API_DB_NAME",
                        help="Database to post data to.")
    parser.add_argument("-a", "--db-authentication-database",
                        type=str,
                        default="rosa",
                        env_var="API_DB_AUTHENTICATION_DATABASE",
                        help="Database to authenticate to.")
    parser.add_argument("-C", "--db-collection-name",
                        type=str,
                        default="test",
                        env_var="API_DB_COLLECTION_NAME",
                        help="Database collection to dump data to")
    parser.add_argument("-V", "--version",
                        type=str,
                        default="unknown",
                        env_var="API_VERSION",
                        help="The api version number to broadcast")
    args = vars(parser.parse_args(argv))
    return args
