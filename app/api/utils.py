# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T12:12:50+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-16T12:13:05+01:00
# @License: please see LICENSE file in project root


def merge_dictionary(*dicts, to_copy=True):
    """Given multiple dictionaries, merge together in order.

    :param *dicts: dictionaries merged from low to high priority.
    :type *dicts: tuple
    :return: None.
    :rtype: None
    """
    result = {}
    for dictionary in dicts:
        result.update(dictionary)  # merge each dictionary in order
    return result
