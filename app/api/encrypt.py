# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T12:08:24+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-16T12:13:26+01:00
# @License: please see LICENSE file in project root
import flask
import flask_restful  # for api
from .users import is_authenticated
import datetime
from fhe.reseal import Reseal, ReScheme
import marshmallow
from .utils import merge_dictionary


class Encrypt(flask_restful.Resource):

    def put(self):
        time_recieved = datetime.datetime.utcnow()
        data = flask.request.data
        dict_schema = {
            "name": marshmallow.fields.Str(),
            "pass": marshmallow.fields.Str(),
            "data": marshmallow.fields.List(marshmallow.fields.Float()),
            "options": marshmallow.fields.Dict()
        }
        args = marshmallow.Schema.from_dict(dict_schema)().loads(data)
        authenticated = is_authenticated(args["name"], args["pass"])
        if authenticated:
            defaults = {
                "scheme": 2,
                "poly_modulus_degree": 8192,
                "coefficient_modulus": [60, 40, 40, 60],
                "scale": pow(2.0, 40),
                "cache": True,
            }
            if args.get("options"):
                defaults = merge_dictionary(defaults, args["options"])
            r = Reseal(**defaults)
            r.ciphertext = args["data"]
            reseal_dict = r.__getstate__()
            return {"data": ReScheme().dumps(reseal_dict),
                    "time_server_recieved": str(time_recieved),
                    "time_server_responded": str(datetime.datetime.utcnow())}
        else:
            return {"message":
                    "error, user '{}' failed authentication".format(
                        args["name"])}, 403
